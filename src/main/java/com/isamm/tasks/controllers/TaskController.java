package com.isamm.tasks.controllers;

import com.isamm.tasks.dto.DatatablesRequest;
import com.isamm.tasks.dto.LabelDTO;
import com.isamm.tasks.dto.TaskDT;
import com.isamm.tasks.dto.TaskDTO;
import com.isamm.tasks.models.Label;
import com.isamm.tasks.models.TaskStatistics;
import com.isamm.tasks.services.LabelService;
import com.isamm.tasks.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/tasks")
public class TaskController {

    private final TaskService taskService;
    @Autowired
    private LabelService labelService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping
    public ResponseEntity<TaskDTO> createTask(@RequestBody TaskDTO taskDTO) {
        TaskDTO task = taskService.save(taskDTO);
        return ResponseEntity.ok().body(task);
    }

    @GetMapping
    public ResponseEntity<List<TaskDTO>> findAllTasks() {
        List<TaskDTO> tasks = taskService.findAll();
        return ResponseEntity.ok().body(tasks);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TaskDTO> findOneTask(@PathVariable Long id) {
        TaskDTO task = taskService.findOne(id);
        return ResponseEntity.ok().body(task);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTask(@PathVariable Long id) {
        taskService.delete(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping
    public ResponseEntity<TaskDTO> updateTask(@RequestBody TaskDTO taskDTO) {
        if (taskDTO.getId() == null) {
            return createTask(taskDTO);
        } else {

            TaskDTO task = taskService.save(taskDTO);
            return ResponseEntity.ok().body(task);
        }
    }

    @PutMapping("/{taskId}/assignLabel/{labelId}")
    public ResponseEntity<TaskDTO> assignLabelToTask(@PathVariable Long taskId, @PathVariable Long labelId) {
        TaskDTO task = taskService.findOne(taskId);
        LabelDTO label = labelService.findOne(labelId);

        if (task != null && label != null) {
            List<LabelDTO> taskLabels = task.getLabels();
            taskLabels.add(label);
            task.setLabels(taskLabels);
            taskService.save(task);
            return ResponseEntity.ok().body(task);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/{taskId}/markAsCompleted")
    public ResponseEntity<TaskDTO> markTaskAsCompleted(@PathVariable Long taskId) {
        TaskDTO task = taskService.findOne(taskId);

        if (task != null) {
            task.setCompleted(true);
            taskService.save(task);
            return ResponseEntity.ok().body(task);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/{projectId}/tasks")
    public ResponseEntity<List<TaskDTO>> getTasksByProject(@PathVariable Long projectId) {
        List<TaskDTO> tasks = taskService.getTasksByProject(projectId);

        if (tasks.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok().body(tasks);
        }
    }

    @GetMapping("/label/{labelId}/tasks")
    public ResponseEntity<List<TaskDTO>> getTasksByLabel(@PathVariable Long labelId) {
        List<TaskDTO> tasks = taskService.getTasksByLabel(labelId);

        if (tasks.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok().body(tasks);
        }
    }

    @PutMapping("/{id}/to-trash")
    public ResponseEntity<TaskDTO> toTrash(@PathVariable Long id) {
        TaskDTO task = taskService.toTrash(id);
        return ResponseEntity.ok().body(task);
    }

    @PutMapping("/{id}/to-list-task")
    public ResponseEntity<TaskDTO> toListTask(@PathVariable Long id) {
        TaskDTO task = taskService.toListTask(id);
        return ResponseEntity.ok().body(task);
    }

    @GetMapping("/filterByLabel")
    public ResponseEntity<List<TaskDTO>> filterTasksByLabelAndFutureDueDate(@RequestParam Long labelId) {
        List<TaskDTO> filteredTasks = taskService.getTasksByLabelFiltred(labelId);

        if (filteredTasks.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok().body(filteredTasks);
        }
    }

    @GetMapping("/filterByLabelAndProject")
    public ResponseEntity<List<TaskDTO>> filterTasksByLabelAndProject(
            @RequestParam Long labelId,
            @RequestParam Long projectId
    ) {
        List<TaskDTO> filteredTasks = taskService.getTasksByLabelAndProject(labelId, projectId);

        if (filteredTasks.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok().body(filteredTasks);
        }
    }

    @GetMapping("/filterByDueDate")
    public ResponseEntity<List<TaskDTO>> filterTasksByDueDate(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dueDate) {
        List<TaskDTO> filteredTasks = taskService.getTasksByDueDate(dueDate);

        if (filteredTasks.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok().body(filteredTasks);
        }
    }

    @GetMapping("/filterByDueDateAndProject")
    public ResponseEntity<List<TaskDTO>> filterTasksByDueDateAndProject(
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dueDate,
            @RequestParam Long projectId
    ) {
        List<TaskDTO> filteredTasks = taskService.getTasksByDueDateAndProjectSortedByDueDateDescending(dueDate, projectId);

        if (filteredTasks.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok().body(filteredTasks);
        }
    }

    @GetMapping("/filterByStartDateAndProject")
    public ResponseEntity<List<TaskDTO>> filterTasksByStartDateAndProject(
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam Long projectId
    ) {
        List<TaskDTO> filteredTasks = taskService.getTasksByStartDateAndProjectSortedByStartDateAscending(startDate, projectId);

        if (filteredTasks.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok().body(filteredTasks);
        }
    }

    @GetMapping("/filterByCompletedAndProject")
    public ResponseEntity<List<TaskDTO>> filterTasksByCompletedAndProject(
            @RequestParam Boolean completed,
            @RequestParam Long projectId
    ) {
        List<TaskDTO> filteredTasks = taskService.getTasksByCompletedAndProjectSortedByDueDateAscending(completed, projectId);

        if (filteredTasks.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok().body(filteredTasks);
        }
    }

    @GetMapping("/labels")
    public ResponseEntity<Map<Label, Long>> getTaskCountByLabelInProject(@PathVariable Long projectId) {
        Map<Label, Long> taskCounts = taskService.countTasksByProjectId(projectId);
        return ResponseEntity.ok().body(taskCounts);
    }
    
    /*@GetMapping("/search")
    public ResponseEntity<List<TaskDTO>> searchTaskByName(
            @RequestParam(required = false) String keyword) {
	  List<TaskDTO> tasks = taskService.searchTasksByName(keyword);
      return ResponseEntity.ok().body(tasks);
}*/


    @GetMapping("/filter")
    public ResponseEntity<List<TaskDTO>> filter(
            @RequestParam(required = false) List<Long> labelIds,
            @RequestParam(required = false) String keyword,
            @RequestParam(required = false) Long projectId,
            @RequestParam(required = false) Boolean completed,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate minStartDate,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate maxStartDate,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate minDueDate,
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate maxDueDate) {

        List<TaskDTO> tasks = taskService.filter(
                labelIds,
                projectId,
                keyword,
                completed,
                minStartDate,
                maxStartDate,
                minDueDate,
                maxDueDate
        );
        return ResponseEntity.ok().body(tasks);
    }


    @GetMapping("/getByStatusAndMemberId")
    public ResponseEntity<List<TaskDTO>> getTasksByStatusAndMember(
            @RequestParam String status,
            @RequestParam Long memberId) {
        try {
            List<TaskDTO> tasks = taskService.getTasksByStatusAndMembers_Id(status, memberId);
            return ResponseEntity.ok(tasks);
        } catch (Exception e) {
            // Handle exceptions and return an appropriate response
            return ResponseEntity.status(400).body(null);
        }
    }

    private boolean isTaskDelayed(TaskDTO task) {
        return ("IN_PROGRESS".equals(task.getStatus()) || "PENDING".equals(task.getStatus())) && LocalDate.now().isAfter(task.getDueDate());
    }

    @GetMapping("/statistics")
    public TaskStatistics getTaskStatistics() {
        List<TaskDTO> tasks = taskService.findAll();
        long allTasksCount = tasks.size();
        long doneTasksCount = tasks.stream().filter(task -> "DONE".equals(task.getStatus())).count();
        long delayedTasksCount = tasks.stream().filter(task -> isTaskDelayed(task)).count();

        return new TaskStatistics(doneTasksCount, delayedTasksCount, allTasksCount);
    }

   /* @GetMapping("/search")
    public ResponseEntity<Map<String, Object>> getTasksWithSearch(
            @RequestParam(name = "searchTerm", required = false) String searchTerm,
            Pageable pageable) {
        try {
            Page<TaskDTO> tasks = taskService.getTasksWithSearch(searchTerm, pageable);

            Map<String, Object> response = new HashMap<>();
            response.put("data", tasks.getContent());
            response.put("recordsTotal", tasks.getTotalElements());
            response.put("recordsFiltered", tasks.getTotalElements()); // Assuming the same for simplicity


            return ResponseEntity.ok(response);
        } catch (Exception e) {
            // Log the exception or handle it appropriately
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }*/

    @PostMapping("/search")
    public ResponseEntity<TaskDT> getTasksWithSearch(@RequestBody DatatablesRequest datatablesRequest) {
        TaskDT dt = new TaskDT();

        try {
            final int page = (int) Math.ceil((double) datatablesRequest.getStart() / datatablesRequest.getLength());
            final PageRequest pr = PageRequest.of(page, datatablesRequest.getLength());
            final Page<TaskDTO> tasks = taskService.getTasksWithSearch(datatablesRequest.getSearchValue(), pr);
            dt.setRecordsFiltered(tasks.getTotalElements());
            dt.setRecordsTotal(tasks.getTotalElements());
            List<TaskDTO> taskDTOS = tasks.getContent();
            if (!CollectionUtils.isEmpty(taskDTOS)) {
                dt.setData(taskDTOS);
                return ResponseEntity.ok().body(dt);
            } else {
                return ResponseEntity.ok().body(dt);
            }
        } catch (Exception e) {
            // Log the exception or handle it appropriately
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/archived")
    public ResponseEntity<TaskDT> findAllArchived(@RequestBody DatatablesRequest datatablesRequest) {
        TaskDT dt = new TaskDT();
        try {
            final int page = (int) Math.ceil((double) datatablesRequest.getStart() / datatablesRequest.getLength());
            final PageRequest pr = PageRequest.of(page, datatablesRequest.getLength());
            final Page<TaskDTO> tasks = taskService.findAllArchived(pr);
            dt.setRecordsFiltered(tasks.getTotalElements());
            dt.setRecordsTotal(tasks.getTotalElements());
            List<TaskDTO> taskDTOS = tasks.getContent();
            if (!CollectionUtils.isEmpty(taskDTOS)) {
                dt.setData(taskDTOS);
                return ResponseEntity.ok().body(dt);
            } else {
                return ResponseEntity.ok().body(dt);
            }
        } catch (Exception e) {
            // Log the exception or handle it appropriately
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}